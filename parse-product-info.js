const XlsxPopulate = require('xlsx-populate');
const getCategories = require('./product-helpers/breadcrumbs');
const getDescription = require('./product-helpers/description');
const downloadAllProductPhotos = require('./product-helpers/product-photos');

module.exports = async (page, link) => {
  await page.goto(link)

  const productName = await page.$eval('.catalog-detail > h1.catalog-detail__title', h1 => h1.innerText)
  const productCategories = await getCategories(page)
  const productDescription = await getDescription(page)
  let productCode = await page.$eval('.catalog-detail__prop', div => div.innerText)
  productCode = productCode.split(' ')[1]

  if (!productCode) return

  await downloadAllProductPhotos(page, productCode)

  const xlsx = await XlsxPopulate.fromFileAsync('./products.xlsx')
  const sheet = xlsx.sheet(0)
  const rowNumber = sheet._rows.length

  sheet.cell(`A${rowNumber}`).value(productCode)
  sheet.cell(`B${rowNumber}`).value(productName)
  sheet.cell(`H${rowNumber}`).value(productDescription)

  productCategories.forEach((category, index) => {
    sheet.cell(rowNumber, index + 3).value(category)
  })

  await xlsx.toFileAsync('./products.xlsx')

  console.log(`${productCode} Done`)
}

module.exports = async (page) => {
  try {
    let description = await page.$eval('.d-tab__content > .d-tab__content-item.active', div => div.innerText)
    description = description.trim()

    return description
  } catch (err) {
    return null
  }
}

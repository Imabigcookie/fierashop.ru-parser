module.exports = async (page) => {
  let allCharacteristics
  const characteristicKeys = await page.$$eval('#elementProperties > .stats > tbody > tr > .name > span', keys => keys.map(key => key.innerText))
  const characteristicValues = await page.$$eval(
    '#elementProperties > table > tbody > tr > td:nth-child(2)',
    values => values.map(value => {
      const childrenLength = value.children.length

      if (!childrenLength) return value.innerText

      return value.children[0].innerText
    })
  )

  allCharacteristics = characteristicKeys.reduce((acc, key, index) => {
    acc[key] = characteristicValues[index]

    return acc
  }, {})

  return allCharacteristics
}

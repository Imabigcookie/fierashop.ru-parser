const fs = require('fs');
const axios = require('axios');
const path = require('path')

downloadImage = async (link, index, productCode) => {
  const url = link
  const fileExt = link.split('.')[2]
  const filePath = path.resolve(`documents/${productCode}/images`, `image_${index}.${fileExt}`)
  const writer = fs.createWriteStream(filePath)

  const response = await axios({
    url,
    method: 'GET',
    responseType: 'stream',
  })

  response.data.pipe(writer)

  return new Promise((res, rej) => {
    writer.on('finish', res)
    writer.on('error', rej)
  })
}

module.exports = async (page, productCode) => {
  const downloadLinksArray = await page.$$eval('.imagesnew_dotted.slick-initialized.slick-slider .slick-slide img.catalog-detail__g-img', images => images.map(image => image.src))

  const filteredLinksArray = downloadLinksArray.filter(link => link !== 'https://fierashop.ru/upload/none.jpg')
  const dirPath = path.resolve(`documents/${productCode}/images`)

  fs.mkdirSync(dirPath, { recursive: true })

  await Promise.all(filteredLinksArray.map((link, index) => downloadImage(link, index + 1, productCode)))
}

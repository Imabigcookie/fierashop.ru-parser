const fs = require('fs');
const axios = require('axios');
const path = require('path');

downloadFile = async (link, productCode) => {
  const url = link
  const fileName = link.split('/').slice(-1).pop()
  const filePath = path.resolve(`documents/${productCode}/files`, fileName)
  const writer = fs.createWriteStream(filePath)

  const response = await axios({
    url,
    method: 'GET',
    responseType: 'stream',
  })

  response.data.pipe(writer)

  return new Promise((res, rej) => {
    writer.on('finish', res)
    writer.on('error', rej)
  })
}

module.exports = async (page, productCode) => {
  let downloadLinksArray = []
  const dirPath = path.resolve(`documents/${productCode}/files`)
  try {
    downloadLinksArray = await page.$$eval(
      '#files > .wrap > .items > .item > .tb > .tbr > .icon > a',
      links => links.map(link => link.href)
    )
  } catch (err) {
  }

  fs.mkdirSync(dirPath, { recursive: true })

  await Promise.all(downloadLinksArray.map(link => downloadFile(link, productCode)))
}

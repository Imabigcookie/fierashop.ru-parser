module.exports = async (page) => {
  const allBreadcrumbs = await page.$$eval('.breadcrumb > li span[itemprop="name"]', links => links.map(link => link.innerText))

  allBreadcrumbs.splice(0, 2)

  return allBreadcrumbs
}

const puppeteer = require('puppeteer');
const catalogParser = require('./parse-category');
const clearProject = require('./clear-project');

(async () => {
  await clearProject()

  const browser = await puppeteer.launch()
  const page = await browser.newPage()

  await catalogParser(page, 'https://fierashop.ru/catalog/')

  await browser.close()
  console.log('Done!')
})();

const parseProductInfo = require('./parse-product-info')

parseCategory = async (page, link) => {
  await page.goto(link)

  const productLinks = await page.$$eval('.items-matrix .item .wrap .pic-container', links => links.map(link => link.href))

  for (const productLink of productLinks) {
    await parseProductInfo(page, productLink)
  }

  console.log(`Page ${link} parsed`)

  await page.goto(link)

  try {
    const categoryNextPageLink = await page.$eval('.pagenav > a.next', link => link.href)

    categoryNextPageLink && await parseCategory(page, categoryNextPageLink)
  } catch (err) {
  }
}

module.exports = parseCategory
